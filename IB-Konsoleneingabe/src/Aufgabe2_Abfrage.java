
import java.util.Scanner;

public class Aufgabe2_Abfrage {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
			
		System.out.println("Willkommen User!\n");
		
		System.out.println("Bitte geben Sie ihren Namen ein: ");
		String name = tastatur.next();
		
		
		System.out.println("\nVielen Dank! Geben Sie nun ihr Alter ein: ");
		byte alter = tastatur.nextByte();
		
		System.out.println("Dankeschön! Ihre Eingaben lauten:");
		System.out.println("Ihr Name: " + name);
		System.out.println("Ihr Alter: " + alter);
		
		tastatur.close();
		
	}
}
