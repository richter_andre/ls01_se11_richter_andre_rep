
public class Konsolenausgabe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//�bung 1, Aufgabe 1
		System.out.println("�bung 1, Aufgabe 1:\n");
		
		System.out.print("Ahh, ein Beispielsatz. ");
		System.out.println("\"Oh, noch ein Satz!\"\n");
		
		//Der Unterschied von "print" zu "println" ist, dass "println" einen Absatz hinzuf�gt
		
		//�bung 1, Aufgabe 2
		System.out.println("�bung 1, Aufgabe 2:\n");	
		
		System.out.printf("%7s\n", "*" );
		System.out.printf("%8s\n", "***" );
		System.out.printf("%9s\n", "*****" );
		System.out.printf("%10s\n", "*******" );
		System.out.printf("%11s\n", "*********" );
		System.out.printf("%12s\n", "***********" );
		System.out.printf("%s\n", "*************" );
		System.out.printf("%8s\n", "***" );
		System.out.printf("%8s\n\n", "***");
		
		//�bung 1, Aufgabe 3
		System.out.println("�bung 1, Aufgabe 3:\n");
		
		System.out.printf("%.2f\n", 22.4234234 );
		System.out.printf("%.2f\n", 111.2222 );
		System.out.printf("%.2f\n", 4.0 );
		System.out.printf("%.2f\n", 1000000.551 );
		System.out.printf("%.2f\n\n", 97.34 );
		
		//�bung 2, Aufgabe 1
		System.out.println("�bung 2, Aufgabe 1:\n");
		
		System.out.printf("%5s\n", "**");
		System.out.printf("%s", "*");
		System.out.printf("%8s", "*\n");
		System.out.printf("%s", "*");
		System.out.printf("%8s", "*\n");
		System.out.printf("%7s", "**\n\n");
		
		//�bung 2, Aufgabe 2
		System.out.println("�bung 2, Aufgabe 2:\n");
		
		String gleich = "=";
		String stern = "*";
		
		System.out.printf("%-5d", 0 );
		System.out.printf("%-20s", gleich );
		System.out.printf("%-4s", gleich);
		System.out.printf("%s\n", 1);
		
		System.out.printf("%-5d", 1 );
		System.out.printf("%-2s", gleich);
		System.out.printf("%-18s", 1);
		System.out.printf("%-4s", gleich );
		System.out.printf("%s\n", 1);
		
		
		System.out.printf("%-5d", 2 );
		System.out.printf("%-2s", gleich);
		System.out.printf("%-2s", 1);
		System.out.printf("%-2s", stern);
		System.out.printf("%-14s", 2);
		System.out.printf("%-4s", gleich);
		System.out.printf("%s\n", 2);
		
		System.out.printf("%-5d", 3 );
		System.out.printf("%-2s", gleich);
		System.out.printf("%-2s", 1);
		System.out.printf("%-2s", stern);
		System.out.printf("%-2s", 2);
		System.out.printf("%-2s", stern);
		System.out.printf("%-10s", 3);
		System.out.printf("%-4s", gleich);
		System.out.printf("%s\n", 6);
		
		System.out.printf("%-5d", 4 );
		System.out.printf("%-2s", gleich);
		System.out.printf("%-2s", 1);
		System.out.printf("%-2s", stern);
		System.out.printf("%-2s", 2);
		System.out.printf("%-2s", stern);
		System.out.printf("%-2s", 3);
		System.out.printf("%-2s", stern);
		System.out.printf("%-6s", 4);
		System.out.printf("%-3s", gleich);
		System.out.printf("%s\n", 24);
		
		
		System.out.printf("%-5d", 5 );
		System.out.printf("%-2s", gleich);
		System.out.printf("%-2s", 1);
		System.out.printf("%-2s", stern);
		System.out.printf("%-2s", 2);
		System.out.printf("%-2s", stern);
		System.out.printf("%-2s", 3);
		System.out.printf("%-2s", stern);
		System.out.printf("%-2s", 4);
		System.out.printf("%-2s", gleich);
		System.out.printf("%-2s", 5);
		System.out.printf("%-2s", gleich);
		System.out.printf("%s\n", 120);
		System.out.println();
		
		//�bung 2, Aufgabe 2
		System.out.println("�bung 2, Aufgabe 3:\n");
		
		short fahrenheit, fahrenheit1, fahrenheit2, fahrenheit3, fahrenheit4, fahrenheit5;
		double celsius, celsius1, celsius2, celsius3, celsius4, celsius5;
		
		fahrenheit = 1;
		celsius = 2;
		
		System.out.printf("Fahrenheit  |   Celsius\n");
		System.out.printf("------------------------");
		System.out.println(fahrenheit, celsius);
		
	}

}
