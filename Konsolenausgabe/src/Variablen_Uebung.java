
public class Variablen_Uebung {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		 /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
        Vereinbaren Sie eine geeignete Variable */
	  
	  long programmdurchlaeufe;

  /* 2. Weisen Sie dem Zaehler den Wert 25 zu
        und geben Sie ihn auf dem Bildschirm aus.*/
	  
	  programmdurchlaeufe = 25;
	  
	  System.out.println("Programmdurchläufe: " + programmdurchlaeufe);
	  
  /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
        eines Programms ausgewaehlt werden.
        Vereinbaren Sie eine geeignete Variable */
	  
	  	char menu1;
	  	
  /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
        und geben Sie ihn auf dem Bildschirm aus.*/
	  	
	  	menu1 = 'C';

  /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
        notwendig.
        Vereinbaren Sie eine geeignete Variable */
	  	
	  	int astroberechnung;

  /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
        und geben Sie sie auf dem Bildschirm aus.*/

	  	astroberechnung = 300000;
	  	
  /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
        soll die Anzahl der Mitglieder erfasst werden.
        Vereinbaren Sie eine geeignete Variable und initialisieren sie
        diese sinnvoll.*/
	  	
	  	byte mitglieder;
	  	
	  	mitglieder = 7;

  /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
	  	
	  	System.out.println("Vereinsmitlgieder: " + mitglieder);

  /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
        Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
        dem Bildschirm aus.*/
	  	
	  	double elementarladung;
	  	
	  	elementarladung = 1.602E-19;
	  	
	  	System.out.println("Elektrische Elementarladung: " + elementarladung);

  /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
        Vereinbaren Sie eine geeignete Variable. */
	  	
	  	boolean zahlungsabfrage;

  /*11. Die Zahlung ist erfolgt.
        Weisen Sie der Variable den entsprechenden Wert zu
        und geben Sie die Variable auf dem Bildschirm aus.*/
		
	  	zahlungsabfrage = true;
	  	
	  	System.out.println("Zahlung erfolgreich: " + zahlungsabfrage);
	  	
	}

}
