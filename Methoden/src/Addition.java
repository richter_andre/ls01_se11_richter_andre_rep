import java.util.Scanner;

class Addition {

    static Scanner sc = new Scanner(System.in);
    				  //Methodenname
    public static void main(String[] args) { //Methodenkopf
  //Zugriffsmodifizierer 			  //|Argument|
        double zahl1 = 0.0, zahl2 = 0.0, ergebnis = 0.0; //Lokale Variable

        //HINWEIS
        
        hinweis(); //Methodenaufruf

        //EINGABE
        
        zahl1 = eingabe();
        zahl2 = eingabe();

        //BERECHNUNG
        ergebnis = addition(zahl1, zahl2);
        
        //AUSGABE
        
        ausgabe(zahl1, zahl2, ergebnis);
      
    } //Methodenrumpf
    
    //HINWEIS
    public static void hinweis() { //Methodendefinition Zeile 30-38
    	
    	
    	System.out.println("Hinweis: ");
        System.out.println("Das Programm addiert 2 eingegebene Zahlen. ");
        
        return;
    	
    }
    
    //AUSGABE
    public static void ausgabe(double zahl1, double zahl2, double ergebnis) {  //Parameter
    	
 
    	System.out.println("Ergebnis der Addition:");
        System.out.printf("%.2f + %.2f = %.2f", zahl1, zahl2, ergebnis);
        
        return;
    	
    }
    
    //BERECHNUNG
    public static double addition(double zahl1, double zahl2) {
    	
    	double addition; //R�ckgabedatentyp
    	
    	addition = zahl1 + zahl2;
    
    	return addition; //R�ckgabewert
    	
    }
    
    //EINGABE
    public static double eingabe() {
    	
    	double zahl1;
    	
    	System.out.println("Geben Sie eine Zahl ein:");
        zahl1 = sc.nextDouble();
        
        return zahl1;
    	
    }
    
}
