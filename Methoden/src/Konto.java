import java.util.Scanner;

public class Konto {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		double konto, einzahlung, auszahlung;
		String navigation;
		boolean vorgang = true;
		boolean eingabe = true;
		char weiter;
		
		konto = 0;
		einzahlung = 0;
		auszahlung = 0;
		
		System.out.println("Sch�nen guten Tag!");
		
		// Navigation ANFANG
		while(vorgang == true) {
			System.out.println("W�hlen Sie, welche Operation Sie nutzen wollen:");
			System.out.println("1 | Kontostand abfragen");
			System.out.println("2 | Geld auf das Konto einzahlen");
			System.out.println("3 | Geld von dem Konto abbuchen");
			
			System.out.println("Ihre Eingabe: ");
			navigation = sc.next();
			switch (navigation)			{
				case "1":
					
					kontostand(konto);
					
					break;
					
				case "2":
					
					einzahlung(konto, einzahlung, sc);
					
					break;
					
				case "3":
					
					auszahlung(konto, auszahlung, sc);
					
					break;
			}
			eingabe = true;
			
			
			while (eingabe == true) {
				
				System.out.println("M�chten Sie eine weitere Operation t�tigen?");
				System.out.println("Y = Ja, N = Nein");
				System.out.println("Ihre Eingabe: ");
				
				weiter = sc.next().charAt(0);
				
				if(weiter == 'N' || weiter == 'n') {
					
					vorgang = false;
					eingabe = false;
					
					System.out.println("Okay, wir w�nschen ihnen einen guten Tag!");
				}
				
				else if(weiter == 'Y' || weiter == 'y') {
					
					eingabe = false;
					
				}
			
				else {
						
						System.out.println("Bitte geben Sie eine korrekte Eingabe ein:");
						
					
				}	
				
				/* else if(weiter != 'N' || weiter != 'n'|| weiter != 'Y' || weiter != 'y'){
					
					System.out.println("Bitte geben Sie eine korrekte Eingabe ein:");
					
				
			} */	
		
			}
		
		// Navigation ENDE
		
		//konto = einzahlung(konto, einzahlung, sc);
		//konto = auszahlung(konto, auszahlung, sc);
		
		//Weitere Operation?
		
		
			}
		}
	public static void kontostand(double konto) {
		
		System.out.println("Ihr akutteller Kontostand betr�gt:" + konto + "�");
	
		
	}

	public static double einzahlung(double konto, double einzahlung, Scanner sc) {

		System.out.println("Welche Summe m�chten Sie auf ihr Konto einzahlen?");
		
		einzahlung = sc.nextDouble();
		
		konto += einzahlung;
		
		System.out.println("Vielen Dank, auf das Konto werden " + einzahlung + "� eingezahlt.");

		System.out.println("Ihr aktueller Kontostand betr�gt: " + konto + "�");
		
		einzahlung = 0;
		
		return konto;
		
	}
	
	public static double auszahlung(double konto, double auszahlung, Scanner sc) {
		
		System.out.println("Welche Summe m�chten Sie von ihrem Konto abbuchen?");
		
		auszahlung = sc.nextDouble();
		
		konto -= auszahlung;
		
		System.out.println("Vielen Dank, auf das Konto werden " + auszahlung + "� abgebucht.");
		
		System.out.println("Ihr aktueller Kontostand betr�gt: " + konto + "�");
		
		auszahlung = 0;
		
		return konto;
		
		
	}
	
	

}