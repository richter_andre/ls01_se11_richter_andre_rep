
import java.util.Scanner;

public class Methoden {

	public static void main(String[] args) {
	
	double zahl1;
	double zahl2;
	double ergebnis;
		
	Scanner Tastatur = new Scanner (System.in);
	
	System.out.println("Bitte geben Sie die erste Zahl zur Berechnung des Durschnitts an: ");
	zahl1 = Tastatur.nextDouble();

	System.out.println("Vielen Dank! Geben Sie nun die zweite Zahl an: ");
	zahl2 = Tastatur.nextDouble();
	
	ergebnis = berechnung(zahl1, zahl2);

	System.out.println("Ihr Ergebnis lautet: " + ergebnis);
	
	}

	static double ergebnis;
	
	public static double berechnung(double zahl1, double zahl2) {
	
	ergebnis = zahl1 + zahl2 /2;
	
	return ergebnis;
	
	}

}
