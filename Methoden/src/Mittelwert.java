
public class Mittelwert {

   public static void main(String[] args) {

	  double x = 0.0, y = 0.0, m = 0.0;
	  
      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
     
	  eingabe();
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
	  
      verarbeitung();
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      
      ausgabe(x, y, m);

   }
   
   public static void eingabe() {
	   
	   double x = 2.0;
	   double y = 4.0;
	   
	   return;
	   
   }
   
   public static double verarbeitung() {
	   
	   double m = (x + y) / 2.0;
	   
   }
   
   public static void ausgabe(double x, double y, double m) {
	   
	   System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
	   
   }
}

