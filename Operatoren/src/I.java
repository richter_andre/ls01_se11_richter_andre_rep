
public class I {

	public static void main(String[] args) {

		int c = 10, d = 5;
		char buchstabe = 'f';
		boolean e = true;
		
		//1.
		
		System.out.println(c < 9 || d >= 11);
		
		//2.
		
		System.out.println(c > 9 || (d > 7 && buchstabe == 'f'));

		//3.
		
		System.out.println(
				(c < 5 || d < 11) && d <= 11);
		
		//4.
		
		System.out.println(!(c == 10) & d <= 11);
		
		//5.
		
		System.out.println(!(c == 10));
		
		//6.
		
		System.out.println(e);
		
		//7.
		
		System.out.println(
				!(c != 10) && ((!(buchstabe != 'f') && c < 11) && !e));
		
	}

}
